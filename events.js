// Copyright Free - Andrew Charnley, http://www.andrewcharnley.com
// You'll need to remove or customise the igaro.debug lines.

var app = {
    events : {
        listeners : {
            list :  new Array(),
            add : function(event, fn) {
                if (! this.list[event]) this.list[event] = new Array();
                if (!(fn in this.list) && fn instanceof Function) this.list[event].push(fn);
                if (igaro.debug) console.log('EVENTS:APPENDED:'+event);
            },
            remove : function(fn) {
                this.list.forEach(function(event) {
                    for (var i=0; i<this.list[event].length; i++) {
                        if (this.list[event][i] !== fn) continue;
                        if (igaro.debug) console.log('EVENTS:REMOVED:'+event);
                        this.list[event].slice(i,1);
                    }
                });
            }
        },
        dispatch : function(event, params) {
            if (! this.listeners.list[event]) return;
            for (var i=0; i<this.listeners.list[event].length; i++) {
                if (igaro.debug) console.log('EVENTS:DISPATCH:'+event);
                this.listeners.list[event][i].call(window,params);
            }
        }
    }
}