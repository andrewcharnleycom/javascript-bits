// Copyright Free - Andrew Charnley, http://www.andrewcharnley.com
// Parses a parameter out of the URL

var app = {
    url : {
	    param : {
            get : function(name) {
                name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                var regexS = "[\\?&]"+name+"=([^&#]*)";
                var regex = new RegExp(regexS);
                var results = regex.exec(window.location.href);
                return (results == null)? null:results[1];
            }
        }
    }
}